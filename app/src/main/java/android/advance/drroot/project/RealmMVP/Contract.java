package android.advance.drroot.project.RealmMVP;

import java.util.List;

public interface Contract {
    interface View{
        void onUserLoaded(List<StudentEntity> students);
        void userRegister();
    }
    interface Presenter{
        void attachViwe(View view);
        void register(String name , String family,String mobile, String age);
        void onUserLoaded(List<StudentEntity> students);
        void userRegister();
        void userEdit(StudentEntity students);
        void userDelet(StudentEntity students);
    }
    interface Model{
        void attachPresenter(Presenter presenter);
        void loadList();
        void register(String name , String family,String mobile, String age);
        void userDelet(StudentEntity students);
        void userEdit(StudentEntity students);
    }
}
