
package android.advance.drroot.project.MyRest.Models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SuccessEntity {

    @SerializedName("action")
    private String mAction;
    @SerializedName("token")
    private String mToken;

    public String getAction() {
        return mAction;
    }

    public void setAction(String action) {
        mAction = action;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        mToken = token;
    }

}
