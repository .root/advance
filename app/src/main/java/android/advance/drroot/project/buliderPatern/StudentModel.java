package android.advance.drroot.project.buliderPatern;

public class StudentModel {

    String name , family;
    int age;
    boolean married;

    public StudentModel(String name, String family, int age, boolean married) {
        this.name = name;
        this.family = family;
        this.age = age;
        this.married = married;
    }

    public StudentModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isMarried() {
        return married;
    }

    public void setMarried(boolean married) {
        this.married = married;
    }

    @Override
    public String toString() {
        return "StudentModel{" +
                "name='" + name + '\'' +
                ", family='" + family + '\'' +
                ", age=" + age +
                ", married=" + married +
                '}';
    }
    public StudentModel setname(String  name){
        this.name = name;
        return this ;
    }

    public StudentModel Family(String family){
        this.family = family;
        return  this ;
    }
    public static void main(String[] args){
        StudentModel std = new StudentModel();
        std.setAge(33);
        std.setName("dasd");
        std.setMarried(true);

        StudentModel stdb = new StudentModel().setname("ali").Family("asd");
    }
}
