package android.advance.drroot.project.CarsMVP;

import java.util.List;

public interface CarContract {
    interface View{
        void onCarsLoaded(List<CarEntity> cars);


    }
    interface Presenter{
        void attachView(View view);
        void onCarsLoaded(List<CarEntity> cars);
        void onCarSelected();
    }

    interface Model{
        void attachPresenter(Presenter presenter);
        void loadCars();
    }
}
