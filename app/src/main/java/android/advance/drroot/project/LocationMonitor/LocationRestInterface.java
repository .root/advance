package android.advance.drroot.project.LocationMonitor;
import android.advance.drroot.project.LocationMonitor.ResultOk.ResultOk;
import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LocationRestInterface {
    //
//    @POST("index1.php")
//    @FormUrlEncoded
//    Call<ResultOk> onLocationSend(
//            @Field("latitude") Double latitude ,
//            @Field("longitude") Double longitude
//                             );
    @POST("index1.php")
    @FormUrlEncoded
    Observable<ResultOk> onLocationSend(
            @Field("latitude") Double latitude,
            @Field("longitude") Double longitude
    );
}
