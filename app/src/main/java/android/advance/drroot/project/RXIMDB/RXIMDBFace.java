package android.advance.drroot.project.RXIMDB;

import android.advance.drroot.project.IMDB_Retrifit.Models.IMDB;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RXIMDBFace {
    @GET("/")
    Observable<IMDB> searchMovie(
            @Query("t") String word
            ,
            @Query("apikey") String  key
    );

}
