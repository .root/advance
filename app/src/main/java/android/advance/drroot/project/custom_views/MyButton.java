package android.advance.drroot.project.custom_views;

import android.advance.drroot.project.utils.BaseApplication;
import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

public class MyButton extends AppCompatButton {
    public MyButton(Context context) {
        super(context);
    this.setTypeface(BaseApplication.typeFace);
    }

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(BaseApplication.typeFace);

    }
}
