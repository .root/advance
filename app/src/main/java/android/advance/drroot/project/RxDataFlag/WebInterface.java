package android.advance.drroot.project.RxDataFlag;

import android.advance.drroot.project.RetrofitSample.Models.FlagModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WebInterface {


    @GET("/")
    Observable<FlagModel> getData(@Query("api-key") String apiKey);
}
