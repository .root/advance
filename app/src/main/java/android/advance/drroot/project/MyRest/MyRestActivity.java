package android.advance.drroot.project.MyRest;

import android.advance.drroot.project.Kotlin.PublicMethods;
import android.advance.drroot.project.R;
import android.advance.drroot.project.custom_views.MyButton;
import android.advance.drroot.project.custom_views.MyEditText;
import android.advance.drroot.project.utils.BaseActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MyRestActivity extends BaseActivity implements Contract.View {
    MyEditText username,password,email;

    Contract.Presenter presenter = new Presenter();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_rest);
        presenter.attachView(this);
        bind();


    }

    private void bind() {
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        email = findViewById(R.id.email);
        findViewById(R.id.register).setOnClickListener(V->{
            presenter.register(username.text(),password.text(),email.text());
        });
        findViewById(R.id.login).setOnClickListener(V->{
            presenter.login(username.text(),password.text());
        });
    }

    @Override
    public void registered() {
        PublicMethods.Companion.toast(mContext , "registered");
        username.setText("");
        password.setText("");
        email.setText("");
    }

    @Override
    public void failed() {
        PublicMethods.Companion.toast(mContext , "failed");

    }

    @Override
    public void loginSuccess() {
        PublicMethods.Companion.toast(mContext , "login success");
    }
}
