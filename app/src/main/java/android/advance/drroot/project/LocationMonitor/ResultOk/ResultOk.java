
package android.advance.drroot.project.LocationMonitor.ResultOk;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ResultOk {

    @SerializedName("result")
    private String mResult;

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
