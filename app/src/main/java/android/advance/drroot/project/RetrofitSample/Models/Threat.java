package android.advance.drroot.project.RetrofitSample.Models;

import com.google.gson.annotations.SerializedName;

public class Threat{

	@SerializedName("is_known_abuser")
	private boolean isKnownAbuser;

	@SerializedName("is_known_attacker")
	private boolean isKnownAttacker;

	@SerializedName("is_anonymous")
	private boolean isAnonymous;

	@SerializedName("is_threat")
	private boolean isThreat;

	@SerializedName("is_bogon")
	private boolean isBogon;

	@SerializedName("is_tor")
	private boolean isTor;

	@SerializedName("is_proxy")
	private boolean isProxy;

	public void setIsKnownAbuser(boolean isKnownAbuser){
		this.isKnownAbuser = isKnownAbuser;
	}

	public boolean isIsKnownAbuser(){
		return isKnownAbuser;
	}

	public void setIsKnownAttacker(boolean isKnownAttacker){
		this.isKnownAttacker = isKnownAttacker;
	}

	public boolean isIsKnownAttacker(){
		return isKnownAttacker;
	}

	public void setIsAnonymous(boolean isAnonymous){
		this.isAnonymous = isAnonymous;
	}

	public boolean isIsAnonymous(){
		return isAnonymous;
	}

	public void setIsThreat(boolean isThreat){
		this.isThreat = isThreat;
	}

	public boolean isIsThreat(){
		return isThreat;
	}

	public void setIsBogon(boolean isBogon){
		this.isBogon = isBogon;
	}

	public boolean isIsBogon(){
		return isBogon;
	}

	public void setIsTor(boolean isTor){
		this.isTor = isTor;
	}

	public boolean isIsTor(){
		return isTor;
	}

	public void setIsProxy(boolean isProxy){
		this.isProxy = isProxy;
	}

	public boolean isIsProxy(){
		return isProxy;
	}

	@Override
 	public String toString(){
		return 
			"Threat{" + 
			"is_known_abuser = '" + isKnownAbuser + '\'' + 
			",is_known_attacker = '" + isKnownAttacker + '\'' + 
			",is_anonymous = '" + isAnonymous + '\'' + 
			",is_threat = '" + isThreat + '\'' + 
			",is_bogon = '" + isBogon + '\'' + 
			",is_tor = '" + isTor + '\'' + 
			",is_proxy = '" + isProxy + '\'' + 
			"}";
		}
}