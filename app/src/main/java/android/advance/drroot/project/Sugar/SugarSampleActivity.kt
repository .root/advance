package android.advance.drroot.project.Sugar

import android.advance.drroot.project.Kotlin.PublicMethods
import android.advance.drroot.project.R
import android.advance.drroot.project.utils.BaseActivity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_sugar_sample.*

class SugarSampleActivity : BaseActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sugar_sample)
        loadStudents()

        register.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        if (view?.id==R.id.register){
            val std = StudentModel()
            std.setName(name.text())
            std.setFamily(family.text())
            std.setAge(age.text().toInt())
            std.save()

            name.setText("")
            family.setText("")
            age.setText("")

            PublicMethods.toast(mContext , "user registered")
            loadStudents()
        }

    }
    fun loadStudents(){
        var Students : List<StudentModel>? =
                StudentModel.listAll(StudentModel::class.java)
        Students?.forEach {

            val res:String = "name is : ${it.getName()}"
            result.append(res)
        }
    }
}
