package android.advance.drroot.project.IMDB_Retrifit;

import android.advance.drroot.project.IMDB_Retrifit.Models.IMDB;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ServiceInterface {

    //?t=thor&apikey=662d6af5

        @GET("/")
    Call<IMDB> searchMovie(
            @Query("t") String word
                ,
            @Query("apikey") String  key
        );
}
