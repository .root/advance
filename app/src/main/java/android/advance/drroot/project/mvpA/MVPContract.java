package android.advance.drroot.project.mvpA;

public interface MVPContract {

    interface View{
        void onAgeReceived(int age);

    }
    interface Presenter{
        void attachView(View view);
        void onGetAge(String name);
        void onAgeReceived(int age);


    }
    interface Model{
        void attachPresenter(Presenter presenter);
        void onGetAge(String name);

    }

}
