package android.advance.drroot.project.buliderPatern;

public class CityModel {
    String name,state;
    boolean isCapital;

    private CityModel(Builder builder) {
        name = builder.name;
        state = builder.state;
        isCapital = builder.isCapital;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private String name;
        private String state;
        private boolean isCapital;

        private Builder() {
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder state(String val) {
            state = val;
            return this;
        }

        public Builder isCapital(boolean val) {
            isCapital = val;
            return this;
        }

        public CityModel build() {
            return new CityModel(this);
        }
    }
    static void test(){
        CityModel city = CityModel.newBuilder()
                .name("tehran")
                .isCapital(true)
                .state("tehran")
                .build();
    }
}
