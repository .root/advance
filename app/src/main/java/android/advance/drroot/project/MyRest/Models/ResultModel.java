
package android.advance.drroot.project.MyRest.Models;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ResultModel {

    @SerializedName("result")
    private String mResult;

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
