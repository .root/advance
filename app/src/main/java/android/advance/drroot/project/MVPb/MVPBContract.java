package android.advance.drroot.project.MVPb;

import android.advance.drroot.project.utils.FlagModels.FlagModels;

public interface MVPBContract {

    interface View{
        void orderDataRecived(FlagModels result);
        void ConnectionFiled();
        void showLoading(boolean show);
    }

    interface Presenter{
        void attachView(View view);
        void orderToGetData();
        void orderDataRecived(FlagModels result);
        void ConnectionFiled();
        void isOnLoading(boolean is);
        }


    interface Model{
        void attachPresenter(MVPBContract.Presenter presenter);
        void orderToGetData();
     }
}
