package android.advance.drroot.project.RealmMVP;

import android.content.Context;
import android.widget.Toast;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class Model implements Contract.Model {
    Context mContext;
    private Contract.Presenter presenter;
    Realm realm = Realm.getDefaultInstance();
    @Override
    public void attachPresenter(Contract.Presenter presenter) {

        this.presenter = presenter;
    }

    @Override
    public void loadList() {
        RealmResults<StudentEntity> students
                = realm.where(StudentEntity.class).findAll();
        presenter.onUserLoaded(students);
    }

    @Override
    public void register(String name, String family, String mobile, String age) {
        StudentEntity std = new StudentEntity(
            name , family , mobile , Integer.parseInt(age));

        realm.beginTransaction();
        realm.copyToRealm(std);
        realm.commitTransaction();
        presenter.userRegister();
    }

    @Override
    public void userDelet(StudentEntity students) {
        realm.beginTransaction();
        RealmResults<StudentEntity> result = realm.where(StudentEntity.class)
                .equalTo("name", students.getName())
                .equalTo("family", students.getFamily())
                .equalTo("age", students.getAge())
                .equalTo("mobile", students.getMobile()
                ).findAll();
        result.deleteFirstFromRealm();
        realm.commitTransaction();
        loadList();

            }
    @Override
    public void userEdit(StudentEntity students) {

    }
 }

