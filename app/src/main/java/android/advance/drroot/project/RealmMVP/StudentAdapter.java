package android.advance.drroot.project.RealmMVP;

import android.advance.drroot.project.R;
import android.advance.drroot.project.custom_views.MyImageView;
import android.advance.drroot.project.custom_views.MyTextView;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.util.List;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.Holder> {
    Context mContext;
    List<StudentEntity> students;

    public StudentAdapter(Context mContext, List<StudentEntity> students) {
        this.mContext = mContext;
        this.students = students;
    }

    @Override
    public StudentAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.std_item , viewGroup ,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentAdapter.Holder holder, int position) {
        holder.name.setText(students.get(position).getName());
        holder.family.setText(students.get(position).getFamily());
        holder.mobile.setText(students.get(position).getMobile());
        holder.age.setText(students.get(position).getAge()+"");
    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        MyTextView name,family,age,mobile;
        ImageButton delet,edit;
        public Holder(@NonNull View itemView) {

            super(itemView);
            name = itemView.findViewById(R.id.name);
            family = itemView.findViewById(R.id.family);
            age = itemView.findViewById(R.id.age);
            mobile = itemView.findViewById(R.id.mobile);
            delet = itemView.findViewById(R.id.delet);
            edit = itemView.findViewById(R.id.edit);
            delet.setOnClickListener(V->{
                StudentEntity selectedDelete = students.get(getAdapterPosition());
                selectDelete.onDeleteSelected(selectedDelete);
            });
            edit.setOnClickListener(V->{
                StudentEntity selectedEdit = students.get(getAdapterPosition());
                selectEdit.onSelectEdit(selectedEdit);
            });
        }
    }
    SelectDelete selectDelete ;

    public void setSelectDelete(SelectDelete selectDelete) {
        this.selectDelete = selectDelete;
    }

    interface SelectDelete{
        void onDeleteSelected(StudentEntity students);
    }
    SelectEdit selectEdit;

    public void setSelectEdit(SelectEdit selectEdit) {
        this.selectEdit = selectEdit;
    }

    interface SelectEdit{
        void onSelectEdit(StudentEntity students);
    }
}
