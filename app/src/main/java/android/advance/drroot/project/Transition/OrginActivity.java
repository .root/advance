package android.advance.drroot.project.Transition;

import android.advance.drroot.project.R;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class OrginActivity extends AppCompatActivity {

    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orgin);
        img = findViewById(R.id.img);
        findViewById(R.id.img).setOnClickListener(V->{

            Intent intent = new Intent(this , DestinationActivity.class);
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this , img , "profile");
            startActivity(intent , options.toBundle());
        });
    }
}
