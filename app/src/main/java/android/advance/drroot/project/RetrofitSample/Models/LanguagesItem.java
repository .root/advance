package android.advance.drroot.project.RetrofitSample.Models;

import com.google.gson.annotations.SerializedName;

public class LanguagesItem{

	@SerializedName("native")
	private String jsonMemberNative;

	@SerializedName("name")
	private String name;

	public void setJsonMemberNative(String jsonMemberNative){
		this.jsonMemberNative = jsonMemberNative;
	}

	public String getJsonMemberNative(){
		return jsonMemberNative;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	@Override
 	public String toString(){
		return 
			"LanguagesItem{" + 
			"native = '" + jsonMemberNative + '\'' + 
			",name = '" + name + '\'' + 
			"}";
		}
}