package android.advance.drroot.project.RetrofitSample;

import android.advance.drroot.project.Kotlin.PublicMethods;
import android.advance.drroot.project.R;
import android.advance.drroot.project.RetrofitSample.Models.FlagModel;
import android.advance.drroot.project.utils.BaseActivity;
import android.advance.drroot.project.utils.RetrofitGenerator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitActivity extends BaseActivity {

    FlagInterface flagInterface = RetrofitGenerator
            .createService(FlagInterface.class);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrofit);

        findViewById(R.id.get).setOnClickListener(V->{
                callService();

        });
    }
            void callService(){

        flagInterface.getFlag("b8d26c545840262364751f1a601aeb5b5e689c47b426f320f6e2beeb")
                .enqueue(new Callback<FlagModel>() {
                    @Override
                    public void onResponse(Call<FlagModel> call, Response<FlagModel> response) {
                        PublicMethods.Companion.toast(mContext ,response.body().getCountryName());
                    }

                    @Override
                    public void onFailure(Call<FlagModel> call, Throwable t) {

                        PublicMethods.Companion.toast(mContext , "failed");
                    }
                });
    }
}
