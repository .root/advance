package android.advance.drroot.project.IMDB_Retrifit;

import android.advance.drroot.project.IMDB_Retrifit.Models.IMDB;
import android.advance.drroot.project.Kotlin.PublicMethods;
import android.advance.drroot.project.R;
import android.advance.drroot.project.custom_views.MyEditText;
import android.advance.drroot.project.custom_views.MyImageView;
import android.advance.drroot.project.utils.BaseActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.orhanobut.logger.Logger;
import com.squareup.picasso.Picasso;

public class RetroActivity extends BaseActivity implements Contract.View {

    MyImageView poster;
    MyEditText movie ;
    TextView name , actor,title;
    Presenter presenter = new Presenter(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retro);
        bind();
    }
    void bind(){
        poster = findViewById(R.id.poster);
        actor = findViewById(R.id.actor);
        title = findViewById(R.id.title);
        movie = findViewById(R.id.movie);
        name = findViewById(R.id.name);
        findViewById(R.id.search).setOnClickListener(V->{
            presenter.search(movie.text());
        });
    }

    @Override
    public void failed() {
        PublicMethods.Companion.toast(mContext , "Failed");
    }

    @Override
    public void movieFound(IMDB result) {

        poster.load(result.getPoster());

        actor.setText(result.getActors());
        title.setText(result.getTitle());
        name.setText(result.getCountry());

        Logger.d(result);
    }
}
