package android.advance.drroot.project.MyRest;

public interface Contract {

    interface View{
        void registered();
        void failed();
        void loginSuccess();
    }
    interface Presenter{
        void attachView(View view);
        void register(String username,String password,String email);
        void registered();
        void failed();
        void login(String username,String password);
        void loginSuccess(String token);
    }
    interface Model{
        void attachPresenter(Presenter presenter);
        void register(String username,String password,String email);
        void login(String username,String password);
        void storeToken(String token);
    }
}
