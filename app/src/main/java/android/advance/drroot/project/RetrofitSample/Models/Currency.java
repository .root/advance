package android.advance.drroot.project.RetrofitSample.Models;

import com.google.gson.annotations.SerializedName;

public class Currency{

	@SerializedName("symbol")
	private String symbol;

	@SerializedName("code")
	private String code;

	@SerializedName("plural")
	private String plural;

	@SerializedName("native")
	private String jsonMemberNative;

	@SerializedName("name")
	private String name;

	public void setSymbol(String symbol){
		this.symbol = symbol;
	}

	public String getSymbol(){
		return symbol;
	}

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setPlural(String plural){
		this.plural = plural;
	}

	public String getPlural(){
		return plural;
	}

	public void setJsonMemberNative(String jsonMemberNative){
		this.jsonMemberNative = jsonMemberNative;
	}

	public String getJsonMemberNative(){
		return jsonMemberNative;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	@Override
 	public String toString(){
		return 
			"Currency{" + 
			"symbol = '" + symbol + '\'' + 
			",code = '" + code + '\'' + 
			",plural = '" + plural + '\'' + 
			",native = '" + jsonMemberNative + '\'' + 
			",name = '" + name + '\'' + 
			"}";
		}
}