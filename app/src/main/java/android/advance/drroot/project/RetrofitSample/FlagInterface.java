package android.advance.drroot.project.RetrofitSample;

import android.advance.drroot.project.RetrofitSample.Models.FlagModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FlagInterface {
    //?api-key=b8d26c545840262364751f1a601aeb5b5e689c47b426f320f6e2beeb

    @GET("/")
    Call<FlagModel> getFlag(@Query("api-key") String apiKey) ;


}
