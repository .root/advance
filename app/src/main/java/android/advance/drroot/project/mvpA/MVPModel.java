package android.advance.drroot.project.mvpA;

public class MVPModel implements MVPContract.Model {

    MVPContract.Presenter presenter;
    @Override
    public void attachPresenter(MVPContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onGetAge(String name) {
        int age = 0;
        if (name.equals("mohamad"))
            age = 24;
        else if (name.equals("yaqob"))
            age = 30;
        else
            age = 50;

        presenter.onAgeReceived(age);

    }
}
