package android.advance.drroot.project.CarsMVP;

import java.util.ArrayList;
import java.util.List;

public class CarModel implements CarContract.Model {
    private CarContract.Presenter presenter;

    @Override
    public void attachPresenter(CarContract.Presenter presenter) {

        this.presenter = presenter;
    }

    @Override
    public void loadCars() {
        List<CarEntity> cars = new ArrayList<>();

        CarEntity pride = new CarEntity();
        pride.setName("pride");
        pride.setPrice(40);
        pride.setCompany("Saipa");
        pride.setImg("https://digiato.com/wp-content/uploads/2018/05/pride1-800x450.jpg");

        CarEntity p206 = new CarEntity();
        p206.setName("206");
        p206.setPrice(80);
        p206.setCompany("pejot");
        p206.setImg("https://i.pinimg.com/originals/87/d9/5c/87d95cab5fca173ee96243e17ca095bc.jpg");

        cars.add(pride);
        cars.add(p206);
        cars.add(pride);
        cars.add(p206);
        cars.add(pride);
        cars.add(p206);
        cars.add(pride);
        cars.add(p206);
        cars.add(pride);
        cars.add(p206);
        cars.add(pride);
        cars.add(p206);
        cars.add(pride);
        cars.add(p206);
        cars.add(pride);
        cars.add(p206);
        cars.add(pride);
        cars.add(p206);
        cars.add(pride);
        cars.add(p206);
        presenter.onCarsLoaded(cars);

    }
}
