package android.advance.drroot.project.Project;

import android.advance.drroot.project.R;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class RegisterForm extends AppCompatActivity {
    ImageView image;
    ImageButton addimg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_form);
        image = findViewById(R.id.image);
        addimg = findViewById(R.id.addimg);
        addimg.setOnClickListener(V -> {

            AlertDialog mBulider = new AlertDialog.Builder(RegisterForm.this).create();
            View mView = getLayoutInflater().inflate(R.layout.dialog, null);
            mBulider.setView(mView);
            mBulider.show();
            Button imageBtn = (Button) mView.findViewById(R.id.imagedialog);
            Button cameraBtn = (Button) mView.findViewById(R.id.cameradialog);
            imageBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,
                            "Select Picture"), 101);

                    addimg.setVisibility(
                            View.GONE
                    );
                    mBulider.dismiss();
                }

            });
            cameraBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, 100);

                    addimg.setVisibility(
                            View.GONE
                    );
                    mBulider.dismiss();

                }

            });

        });
        findViewById(R.id.signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterForm.this , SuccessFulActivity.class);
                startActivity(intent);
            }

        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            Bitmap image = (Bitmap) data.getExtras().get("data");
            ImageView imageview = (ImageView) findViewById(R.id.image);
            imageview.setImageBitmap(image);
        }
        if (requestCode == 101) {

            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                image.setImageBitmap(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }

        }
    }

}
