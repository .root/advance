package android.advance.drroot.project.mvpA;

import android.advance.drroot.project.R;
import android.advance.drroot.project.custom_views.MyEditText;
import android.advance.drroot.project.custom_views.MyTextView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MVPAActivity extends AppCompatActivity implements MVPContract.View {

    MyTextView result ;
    MyEditText name;

    MVPContract.Presenter presenter = new MVPPresenter();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mvpa);

        presenter.attachView(this);
        name = findViewById(R.id.name);
        result = findViewById(R.id.result);

        findViewById(R.id.btn).setOnClickListener(V->{
            presenter.onGetAge(name.text());

        });

    }

    @Override
    public void onAgeReceived(int age) {
        result.setText("age is: " + age);
    }
}
