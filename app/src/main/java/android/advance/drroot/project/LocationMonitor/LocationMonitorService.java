package android.advance.drroot.project.LocationMonitor;
import android.advance.drroot.project.LocationMonitor.ResultOk.ResultOk;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.IBinder;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationMonitorService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
        getLocation();
    }
    void getLocation(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                SmartLocation.with(LocationMonitorService.this)
                        .location()
                        .oneFix()
                        .start(new OnLocationUpdatedListener() {
                            @Override
                            public void onLocationUpdated(Location location) {

                                Double latitude = location.getLatitude();
                                Double longitude = location.getLongitude();
                                LocationRestInterface locationRestInterface = RXRetroLocation.createService(LocationRestInterface.class);
                                locationRestInterface.onLocationSend(latitude , longitude)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(Schedulers.io())
                                        .subscribe(new Observer<ResultOk>() {
                                            @Override
                                            public void onSubscribe(Disposable disposable) {

                                            }

                                            @Override
                                            public void onNext(ResultOk resultOk) {

                                            }

                                            @Override
                                            public void onError(Throwable throwable) {

                                            }

                                            @Override
                                            public void onComplete() {

                                            }
                                        });
//                                locationRestInterface.onLocationSend(latitude , longitude).enqueue(new Callback<ResultOk>() {
//                                    @Override
//                                    public void onResponse(Call<ResultOk> call, Response<ResultOk> response) {
//
//                                    }
//
//                                    @Override
//                                    public void onFailure(Call<ResultOk> call, Throwable t) {
//
//                                    }
//                                });


                            }
                        });
                getLocation();
            }
        }, 6000);

    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
