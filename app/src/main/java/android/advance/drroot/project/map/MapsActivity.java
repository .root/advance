package android.advance.drroot.project.map;

import android.Manifest;
import android.advance.drroot.project.R;
import android.advance.drroot.project.custom_views.MyTextView;
import android.advance.drroot.project.utils.Publics;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.text.format.DateFormat;

import com.google.android.gms.common.api.Response;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;


import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

import static android.icu.util.Calendar.AM;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    MyTextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        txt = findViewById(R.id.txt);
        getPermission();

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        getMyLocation();
        mMap.getUiSettings().setZoomControlsEnabled(true);

        Date time = new Date(new Date().getTime());
        if (time.getTime() < 6 && time.getTime() > 19 ) {

            try {
                mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.mapnight));

            } catch (Exception e) {
            }
        } else {

            try {
                mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map));

            } catch (Exception e) {
            }
        }



        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                getMyLocation();

                marker();


            }
        });

    }

    void getMyLocation() {

        SmartLocation.with(this).location()
                .start(L -> {

                    double lat = L.getLatitude();
                    double lon = L.getLongitude();
                    LatLng myLocation = new LatLng(lat, lon);
                    mMap.addMarker(new MarkerOptions().position(myLocation).title("i am in here"));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));


                });
    }


    void getPermission() {

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION

                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {/* ... */}

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {/* ... */}
        }).check();
    }

    void marker() {
        double lat = mMap.getCameraPosition().target.latitude;
        double lon = mMap.getCameraPosition().target.longitude;
        String address = getAddress(this, lat, lon);

        txt.setText(
//                    lat + " " +lon
                address

        );

    }

    public String getAddress(Context mContext, double lat, double lng) {
        String fullAdd = null;
        try {
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            List<android.location.Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                fullAdd = address.getAddressLine(0);
//                String Location = address.getLocality();
//                String Country = address.getCountryName();
            }
        } catch (Exception e) {

        }
        return fullAdd;
    }
}
