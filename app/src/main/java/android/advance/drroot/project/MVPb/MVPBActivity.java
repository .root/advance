package android.advance.drroot.project.MVPb;

import android.advance.drroot.project.Kotlin.PublicMethods;
import android.advance.drroot.project.R;
import android.advance.drroot.project.custom_views.MyImageView;
import android.advance.drroot.project.custom_views.MyTextView;
import android.advance.drroot.project.utils.BaseActivity;
import android.advance.drroot.project.utils.FlagModels.FlagModels;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MVPBActivity extends BaseActivity implements View.OnClickListener , MVPBContract.View {
    MyImageView flag;
    MyTextView result;
    MVPBContract.Presenter presenter = new MVPBPresenter();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mvpb);
        presenter.attachView(this);
        bind();
    }
    void bind(){
        flag = findViewById(R.id.flag);
        result = findViewById(R.id.result);

        findViewById(R.id.reNew).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        presenter.orderToGetData();
    }

    @Override
    public void orderDataRecived(FlagModels response) {
        flag.load(response.getFlag());
        result.append(response.getIp());
        result.append("\n");
        result.append(response.getCountryName());
        result.append("\n");
        result.append(response.getCity());
        result.append("\n");
        result.append(response.getOrganisation());
    }

    @Override
    public void ConnectionFiled() {
        PublicMethods.Companion.toast(this , "Connection Filed");
    }

    @Override
    public void showLoading(boolean show) {
        if (show)
            dialog.show();
        else
            dialog.dismiss();
    }
}
