package android.advance.drroot.project.recycler;

import android.advance.drroot.project.R;
import android.advance.drroot.project.custom_views.MyImageView;
import android.advance.drroot.project.custom_views.MyTextView;
import android.advance.drroot.project.utils.Publics;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.Holder> {

    List<BookModel> books;
    Context mContext;

    public RecyclerAdapter(Context mContext, List<BookModel> books) {
        this.books = books;
        this.mContext = mContext;
    }

    @Override
    public RecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View v = LayoutInflater.from(mContext).inflate(R.layout.book_recycler_item, viewGroup, false);

        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.Holder holder, int position) {
        holder.name.setText( books.get(position).getName());
        holder.img.load(books.get(position).getImageURL());
    }

    class Holder extends RecyclerView.ViewHolder {
        MyImageView img;
        MyTextView name;

        public Holder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            img = itemView.findViewById(R.id.img);

            img.setOnClickListener(V->{
                BookModel selectedBook = books.get((getAdapterPosition()));
                Publics.toast(selectedBook.getName());

            });
        }
    }

    @Override
    public int getItemCount() {
        return books.size();
    }
}
