package android.advance.drroot.project.CarsMVP;

import android.advance.drroot.project.R;
import android.advance.drroot.project.custom_views.MyImageView;
import android.advance.drroot.project.custom_views.MyTextView;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class CarsAdapter extends RecyclerView.Adapter<CarsAdapter.Holder> {
    Context mContext;
    List<CarEntity> cars;

    public CarsAdapter(Context mContext, List<CarEntity> cars) {
        this.mContext = mContext;
        this.cars = cars;
    }

    @Override
    public CarsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.cars_item , viewGroup ,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CarsAdapter.Holder holder, int position) {
        holder.img.load(cars.get(position).getImg());
        holder.name.setText(cars.get(position).getName());
        holder.company.setText(cars.get(position).getCompany());
        holder.price.setText(cars.get(position).getPrice()+"");
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        MyImageView img;
        MyTextView name,price,company;
        public Holder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img);
            name = itemView.findViewById(R.id.name);
            price = itemView.findViewById(R.id.price);
            company = itemView.findViewById(R.id.company);
            img.setOnClickListener(V->{
                CarEntity selectedCar =
                        cars.get(getAdapterPosition());
                selectCar.onCarSelect(selectedCar);

            });
        }
    }
    SelectCar selectCar;

    public void setSelectCar(SelectCar selectCar) {
        this.selectCar = selectCar;
    }

    interface SelectCar {
        void onCarSelect(CarEntity car);
    }
}
