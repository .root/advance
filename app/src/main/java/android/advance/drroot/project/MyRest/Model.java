package android.advance.drroot.project.MyRest;

import android.advance.drroot.project.Kotlin.PublicMethods;
import android.advance.drroot.project.MyRest.Models.OrderModel;
import android.advance.drroot.project.MyRest.Models.ResultModel;
import android.advance.drroot.project.MyRest.Models.SuccessEntity;
import android.advance.drroot.project.utils.Publics;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Model implements Contract.Model {
    private Contract.Presenter presenter;
    MyRestInterface myRestInterface =  ServiceGeneratorMyRest.createService(MyRestInterface.class);
    @Override
    public void attachPresenter(Contract.Presenter presenter) {

        this.presenter = presenter;
    }

    @Override
    public void register(String username, String password, String email) {

        myRestInterface.register(username, password, email).enqueue(new Callback<ResultModel>() {
            @Override
            public void onResponse(Call<ResultModel> call, Response<ResultModel> response) {
                if (response.body().getResult().equals("ok"))
                presenter.registered();
                else
                    presenter.failed();
            }

            @Override
            public void onFailure(Call<ResultModel> call, Throwable t) {
                    presenter.failed();
            }
        });
    }

    @Override
    public void login(String username, String password) {
        myRestInterface.login(username , password).enqueue(new Callback<SuccessEntity>() {
            @Override
            public void onResponse(Call<SuccessEntity> call, Response<SuccessEntity> response) {
                presenter.loginSuccess(response.body().getToken());
            }

            @Override
            public void onFailure(Call<SuccessEntity> call, Throwable t) {
                presenter.failed();
            }
        });

        myRestInterface.getOrder(PublicMethods.Companion.getData("token" , "")).enqueue(new Callback<List<OrderModel>>() {
            @Override
            public void onResponse(Call<List<OrderModel>> call, Response<List<OrderModel>> response) {

            }

            @Override
            public void onFailure(Call<List<OrderModel>> call, Throwable t) {

            }
        });
    }

    @Override
    public void storeToken(String token) {
        PublicMethods.Companion.storeData("token" , token);
        }


}
