package android.advance.drroot.project.mvpA;

public class MVPPresenter implements MVPContract.Presenter {


    MVPModel model = new MVPModel();
    MVPContract.View view ;

    @Override
    public void attachView(MVPContract.View view) {
        this.view = view;
        model.attachPresenter(this);
    }

    @Override
    public void onGetAge(String name) {

        model.onGetAge(name);
    }
    @Override
    public void onAgeReceived(int age) {

        view.onAgeReceived(age);
    }

}
