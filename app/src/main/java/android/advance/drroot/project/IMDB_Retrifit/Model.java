package android.advance.drroot.project.IMDB_Retrifit;

import android.advance.drroot.project.IMDB_Retrifit.Models.IMDB;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Model implements Contract.Model {
    private Contract.Presenter presenter;

    ServiceInterface serviceInterface = RetroGenerator.createService(ServiceInterface.class);

    @Override
    public void attachPresenter(Contract.Presenter presenter) {

        this.presenter = presenter;
    }

    @Override
    public void search(String word) {

        serviceInterface.searchMovie(word , "662d6af5")
                .enqueue(new Callback<IMDB>() {
                    @Override
                    public void onResponse(Call<IMDB> call, Response<IMDB> response) {
                        presenter.movieFound(response.body());
                    }

                    @Override
                    public void onFailure(Call<IMDB> call, Throwable t) {
                        presenter.failed();
                    }
                });
    }
}
