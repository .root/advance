package android.advance.drroot.project.RXIMDB;

import android.Manifest;
import android.advance.drroot.project.IMDB_Retrifit.Models.IMDB;
import android.advance.drroot.project.Kotlin.PublicMethods;
import android.advance.drroot.project.LocationMonitor.LocationMonitorService;
import android.advance.drroot.project.R;
import android.advance.drroot.project.custom_views.MyEditText;
import android.advance.drroot.project.custom_views.MyImageView;
import android.advance.drroot.project.utils.BaseActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import rx.android.schedulers.AndroidSchedulers;


public class RXIMDBActivity extends BaseActivity {

    MyEditText movie;
    MyImageView poster;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rximdb);
        getPermission();
        bind();

        Intent intent1 = new Intent(this , LocationMonitorService.class);
        this.startService(intent1);
    }

    void bind() {
        movie = findViewById(R.id.movie);
        poster = findViewById(R.id.poster);
        RxTextView.textChanges(movie).
                debounce(1300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onChange);
    }

    private void onChange(CharSequence charSequence) {
        rest(charSequence.toString());
    }
    void rest(String word){

        RXIMDBFace face = RXRetroIMDB.createService(RXIMDBFace.class);
        face.searchMovie(word , "662d6af5")
                .subscribeOn(Schedulers.io())
                .observeOn(io.reactivex.android.schedulers.AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess,this::onError, this::onComplte);
    }

    private void onComplte() {

    }

    private void onError(Throwable throwable) {
        Toast.makeText(mContext, "eror", Toast.LENGTH_SHORT).show();
    }

    private void onSuccess(IMDB imdb) {

        poster.load(imdb.getPoster());
    }
    void getPermission() {

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION

                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {/* ... */}

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {/* ... */}
        }).check();
    }
}