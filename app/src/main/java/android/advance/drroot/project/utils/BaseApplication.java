package android.advance.drroot.project.utils;

import android.app.Application;
import android.graphics.Typeface;

import com.orhanobut.hawk.Hawk;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.BuildConfig;
import com.orhanobut.logger.Logger;
import com.orm.SugarApp;

import io.realm.Realm;

public class BaseApplication extends SugarApp {
    static BaseApplication baseApp ;

    public static Typeface typeFace ;

    @Override
    public void onCreate() {
        super.onCreate();
        baseApp = this ;
        Realm.init(this);
        typeFace =
                Typeface.createFromAsset(getAssets()
                , Constants.appFontName);

        Logger.addLogAdapter(new AndroidLogAdapter() {
            @Override public boolean isLoggable(int priority, String tag) {
                return true;
             }
        });
        Hawk.init(this).build();

    }
}
