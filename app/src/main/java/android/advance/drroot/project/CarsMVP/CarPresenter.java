package android.advance.drroot.project.CarsMVP;



import java.util.ArrayList;
import java.util.List;

public class CarPresenter implements CarContract.Presenter {


    CarModel model = new CarModel();
    private CarContract.View view;

    @Override
    public void attachView(CarContract.View view) {

        this.view = view;
        model.attachPresenter(this);
        model.loadCars();
    }

    @Override
    public void onCarsLoaded(List<CarEntity> cars) {
        view.onCarsLoaded(cars);
    }

    @Override
    public void onCarSelected() {

    }
}
