package android.advance.drroot.project.MVPb;

import android.advance.drroot.project.utils.FlagModels.FlagModels;

public class MVPBPresenter implements MVPBContract.Presenter {

    MVPBContract.View view;
    MVPBContract.Model model = new MVPBModel();

    @Override
    public void attachView(MVPBContract.View view) {
        this.view = view;
        model.attachPresenter(this);
        model.orderToGetData();
    }

    @Override
    public void orderToGetData() {
        model.orderToGetData();
    }

    @Override
    public void orderDataRecived(FlagModels result) {
        view.orderDataRecived(result);
    }

    @Override
    public void ConnectionFiled() {
        view.ConnectionFiled();
    }

    @Override
    public void isOnLoading(boolean is) {
        view.showLoading(is);
    }
}
