package android.advance.drroot.project.recycler;

import android.advance.drroot.project.R;
import android.advance.drroot.project.utils.BaseActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerActivity extends BaseActivity {
        RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);
        bind();

        List<BookModel> books = new ArrayList<>();

        books.add(new BookModel("java" , "https://upload.wikimedia.org/wikipedia/en/thumb/3/30/Java_programming_language_logo.svg/1200px-Java_programming_language_logo.svg.png"));
        books.add(new BookModel("android" , "http://s9.picofile.com/file/8339680750/580b57fbd9996e24bc43bdef.png"));
        books.add(new BookModel("php" , "https://laracasts.com/images/series/circles/solid-principles-in-php.png"));
        books.add(new BookModel("java" , "https://upload.wikimedia.org/wikipedia/en/thumb/3/30/Java_programming_language_logo.svg/1200px-Java_programming_language_logo.svg.png"));
        books.add(new BookModel("android" , "http://s9.picofile.com/file/8339680750/580b57fbd9996e24bc43bdef.png"));
        books.add(new BookModel("php" , "https://laracasts.com/images/series/circles/solid-principles-in-php.png"));
        books.add(new BookModel("java" , "https://upload.wikimedia.org/wikipedia/en/thumb/3/30/Java_programming_language_logo.svg/1200px-Java_programming_language_logo.svg.png"));
        books.add(new BookModel("android" , "http://s9.picofile.com/file/8339680750/580b57fbd9996e24bc43bdef.png"));
        books.add(new BookModel("php" , "https://laracasts.com/images/series/circles/solid-principles-in-php.png"));
        books.add(new BookModel("java" , "https://upload.wikimedia.org/wikipedia/en/thumb/3/30/Java_programming_language_logo.svg/1200px-Java_programming_language_logo.svg.png"));
        books.add(new BookModel("android" , "http://s9.picofile.com/file/8339680750/580b57fbd9996e24bc43bdef.png"));
        books.add(new BookModel("php" , "https://laracasts.com/images/series/circles/solid-principles-in-php.png"));
        books.add(new BookModel("java" , "https://upload.wikimedia.org/wikipedia/en/thumb/3/30/Java_programming_language_logo.svg/1200px-Java_programming_language_logo.svg.png"));
        books.add(new BookModel("android" , "http://s9.picofile.com/file/8339680750/580b57fbd9996e24bc43bdef.png"));
        books.add(new BookModel("php" , "https://laracasts.com/images/series/circles/solid-principles-in-php.png"));
        books.add(new BookModel("java" , "https://upload.wikimedia.org/wikipedia/en/thumb/3/30/Java_programming_language_logo.svg/1200px-Java_programming_language_logo.svg.png"));
        books.add(new BookModel("android" , "http://s9.picofile.com/file/8339680750/580b57fbd9996e24bc43bdef.png"));
        books.add(new BookModel("php" , "https://laracasts.com/images/series/circles/solid-principles-in-php.png"));
        books.add(new BookModel("java" , "https://upload.wikimedia.org/wikipedia/en/thumb/3/30/Java_programming_language_logo.svg/1200px-Java_programming_language_logo.svg.png"));
        books.add(new BookModel("android" , "http://s9.picofile.com/file/8339680750/580b57fbd9996e24bc43bdef.png"));
        books.add(new BookModel("php" , "https://laracasts.com/images/series/circles/solid-principles-in-php.png"));
        books.add(new BookModel("java" , "https://upload.wikimedia.org/wikipedia/en/thumb/3/30/Java_programming_language_logo.svg/1200px-Java_programming_language_logo.svg.png"));
        books.add(new BookModel("android" , "http://s9.picofile.com/file/8339680750/580b57fbd9996e24bc43bdef.png"));
        books.add(new BookModel("php" , "https://laracasts.com/images/series/circles/solid-principles-in-php.png"));

        RecyclerAdapter recyclerAdapter = new RecyclerAdapter(mContext , books);

        recyclerView.setAdapter(recyclerAdapter);

    }

    void bind(){

        recyclerView = findViewById(R.id.recycler);

    }
}
