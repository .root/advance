package android.advance.drroot.project.IMDB_Retrifit;

import android.advance.drroot.project.IMDB_Retrifit.Models.IMDB;

public interface Contract {

    interface View{
        void failed();
        void movieFound(IMDB result);

    }
    interface Presenter{
        void search(String word);
        void failed();
        void movieFound(IMDB result);
    }
    interface Model{
        void attachPresenter(Presenter presenter);
        void search(String word);

    }
}
