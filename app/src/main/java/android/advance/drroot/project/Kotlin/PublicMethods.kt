package android.advance.drroot.project.Kotlin

import android.content.Context
import android.widget.Toast
import com.orhanobut.hawk.Hawk

class PublicMethods {

    companion object {
        fun toast(mContext : Context , msg : String){
            Toast.makeText(mContext , msg , Toast.LENGTH_LONG).show()
        }

        fun storeData(key:String , value:String){
            Hawk.put(key, value)
        }
        fun getData(key:String , defValue:String):String{
            return  Hawk.get(key , defValue)
        }
    }
}