package android.advance.drroot.project.Sugar;

import android.advance.drroot.project.Kotlin.PublicMethods;
import android.advance.drroot.project.R;
import android.advance.drroot.project.custom_views.MyEditText;
import android.advance.drroot.project.custom_views.MyTextView;
import android.advance.drroot.project.utils.BaseActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

public class SugarJavaActivity extends BaseActivity {
    MyEditText name , age , family;
    TextView result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sugar_sample);
        bind();
        loadData();

        findViewById(R.id.register).setOnClickListener(V-> {
            register() ;
        });

    }

    private void register() {
        StudentModel std = new StudentModel();
        try {
            std.setAge(Integer.parseInt(age.text()));


        }catch (Exception e){
            std.setAge(0);
        }
        std.setName(name.text());
        std.setFamily(family.text());
        std.save();
        loadData();
        cleanForm();
        PublicMethods.Companion.toast(mContext , getString(R.string.form_save) );
    }
    void loadData(){

        List<StudentModel> students = StudentModel.listAll(StudentModel.class);
        for (StudentModel std : students){
            result.append(std.getName() + " " + std.getAge());
        }
    }

    void cleanForm(){

        name.setText("");
        family.setText("");
        age.setText("");

    }

    void bind(){
        name = findViewById(R.id.name);
        age = findViewById(R.id.age);
        family = findViewById(R.id.family);
        result = findViewById(R.id.result);
    }
}
