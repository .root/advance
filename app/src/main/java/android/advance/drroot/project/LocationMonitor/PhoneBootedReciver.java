package android.advance.drroot.project.LocationMonitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class PhoneBootedReciver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intent1 = new Intent(context , LocationMonitorService.class);
        context.startService(intent1);
    }
}
