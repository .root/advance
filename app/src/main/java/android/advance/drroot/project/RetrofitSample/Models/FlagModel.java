package android.advance.drroot.project.RetrofitSample.Models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class FlagModel{

	@SerializedName("continent_name")
	private String continentName;

	@SerializedName("calling_code")
	private String callingCode;

	@SerializedName("flag")
	private String flag;

	@SerializedName("languages")
	private List<LanguagesItem> languages;

	@SerializedName("city")
	private String city;

	@SerializedName("ip")
	private String ip;

	@SerializedName("latitude")
	private double latitude;

	@SerializedName("emoji_unicode")
	private String emojiUnicode;

	@SerializedName("count")
	private String count;

	@SerializedName("continent_code")
	private String continentCode;

	@SerializedName("organisation")
	private String organisation;

	@SerializedName("time_zone")
	private TimeZone timeZone;

	@SerializedName("emoji_flag")
	private String emojiFlag;

	@SerializedName("country_code")
	private String countryCode;

	@SerializedName("is_eu")
	private boolean isEu;

	@SerializedName("country_name")
	private String countryName;

	@SerializedName("currency")
	private Currency currency;

	@SerializedName("postal")
	private String postal;

	@SerializedName("threat")
	private Threat threat;

	@SerializedName("region")
	private String region;

	@SerializedName("asn")
	private String asn;

	@SerializedName("region_code")
	private String regionCode;

	@SerializedName("longitude")
	private double longitude;

	public void setContinentName(String continentName){
		this.continentName = continentName;
	}

	public String getContinentName(){
		return continentName;
	}

	public void setCallingCode(String callingCode){
		this.callingCode = callingCode;
	}

	public String getCallingCode(){
		return callingCode;
	}

	public void setFlag(String flag){
		this.flag = flag;
	}

	public String getFlag(){
		return flag;
	}

	public void setLanguages(List<LanguagesItem> languages){
		this.languages = languages;
	}

	public List<LanguagesItem> getLanguages(){
		return languages;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setIp(String ip){
		this.ip = ip;
	}

	public String getIp(){
		return ip;
	}

	public void setLatitude(double latitude){
		this.latitude = latitude;
	}

	public double getLatitude(){
		return latitude;
	}

	public void setEmojiUnicode(String emojiUnicode){
		this.emojiUnicode = emojiUnicode;
	}

	public String getEmojiUnicode(){
		return emojiUnicode;
	}

	public void setCount(String count){
		this.count = count;
	}

	public String getCount(){
		return count;
	}

	public void setContinentCode(String continentCode){
		this.continentCode = continentCode;
	}

	public String getContinentCode(){
		return continentCode;
	}

	public void setOrganisation(String organisation){
		this.organisation = organisation;
	}

	public String getOrganisation(){
		return organisation;
	}

	public void setTimeZone(TimeZone timeZone){
		this.timeZone = timeZone;
	}

	public TimeZone getTimeZone(){
		return timeZone;
	}

	public void setEmojiFlag(String emojiFlag){
		this.emojiFlag = emojiFlag;
	}

	public String getEmojiFlag(){
		return emojiFlag;
	}

	public void setCountryCode(String countryCode){
		this.countryCode = countryCode;
	}

	public String getCountryCode(){
		return countryCode;
	}

	public void setIsEu(boolean isEu){
		this.isEu = isEu;
	}

	public boolean isIsEu(){
		return isEu;
	}

	public void setCountryName(String countryName){
		this.countryName = countryName;
	}

	public String getCountryName(){
		return countryName;
	}

	public void setCurrency(Currency currency){
		this.currency = currency;
	}

	public Currency getCurrency(){
		return currency;
	}

	public void setPostal(String postal){
		this.postal = postal;
	}

	public String getPostal(){
		return postal;
	}

	public void setThreat(Threat threat){
		this.threat = threat;
	}

	public Threat getThreat(){
		return threat;
	}

	public void setRegion(String region){
		this.region = region;
	}

	public String getRegion(){
		return region;
	}

	public void setAsn(String asn){
		this.asn = asn;
	}

	public String getAsn(){
		return asn;
	}

	public void setRegionCode(String regionCode){
		this.regionCode = regionCode;
	}

	public String getRegionCode(){
		return regionCode;
	}

	public void setLongitude(double longitude){
		this.longitude = longitude;
	}

	public double getLongitude(){
		return longitude;
	}

	@Override
 	public String toString(){
		return 
			"FlagModel{" + 
			"continent_name = '" + continentName + '\'' + 
			",calling_code = '" + callingCode + '\'' + 
			",flag = '" + flag + '\'' + 
			",languages = '" + languages + '\'' + 
			",city = '" + city + '\'' + 
			",ip = '" + ip + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",emoji_unicode = '" + emojiUnicode + '\'' + 
			",count = '" + count + '\'' + 
			",continent_code = '" + continentCode + '\'' + 
			",organisation = '" + organisation + '\'' + 
			",time_zone = '" + timeZone + '\'' + 
			",emoji_flag = '" + emojiFlag + '\'' + 
			",country_code = '" + countryCode + '\'' + 
			",is_eu = '" + isEu + '\'' + 
			",country_name = '" + countryName + '\'' + 
			",currency = '" + currency + '\'' + 
			",postal = '" + postal + '\'' + 
			",threat = '" + threat + '\'' + 
			",region = '" + region + '\'' + 
			",asn = '" + asn + '\'' + 
			",region_code = '" + regionCode + '\'' + 
			",longitude = '" + longitude + '\'' + 
			"}";
		}
}