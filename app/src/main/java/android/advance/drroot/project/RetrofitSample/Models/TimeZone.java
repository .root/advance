package android.advance.drroot.project.RetrofitSample.Models;

import com.google.gson.annotations.SerializedName;

public class TimeZone{

	@SerializedName("offset")
	private String offset;

	@SerializedName("name")
	private String name;

	@SerializedName("abbr")
	private String abbr;

	@SerializedName("is_dst")
	private boolean isDst;

	@SerializedName("current_time")
	private String currentTime;

	public void setOffset(String offset){
		this.offset = offset;
	}

	public String getOffset(){
		return offset;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setAbbr(String abbr){
		this.abbr = abbr;
	}

	public String getAbbr(){
		return abbr;
	}

	public void setIsDst(boolean isDst){
		this.isDst = isDst;
	}

	public boolean isIsDst(){
		return isDst;
	}

	public void setCurrentTime(String currentTime){
		this.currentTime = currentTime;
	}

	public String getCurrentTime(){
		return currentTime;
	}

	@Override
 	public String toString(){
		return 
			"TimeZone{" + 
			"offset = '" + offset + '\'' + 
			",name = '" + name + '\'' + 
			",abbr = '" + abbr + '\'' + 
			",is_dst = '" + isDst + '\'' + 
			",current_time = '" + currentTime + '\'' + 
			"}";
		}
}