package android.advance.drroot.project.RxDataFlag;

import android.advance.drroot.project.Kotlin.PublicMethods;
import android.advance.drroot.project.R;
import android.advance.drroot.project.RetrofitSample.Models.FlagModel;
import android.advance.drroot.project.utils.BaseActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RxDataFlagActivity extends BaseActivity {

    WebInterface webInterface =  RXRetroGenarator.createService(WebInterface.class);

    String apiKey = "b8d26c545840262364751f1a601aeb5b5e689c47b426f320f6e2beeb" ;
    Disposable order ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rx_data_flag);

        webInterface.getData(apiKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<FlagModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        order = d;
                    }

                    @Override
                    public void onNext(FlagModel flagModel) {

                        PublicMethods.Companion.toast(mContext, flagModel.getCountryName());
                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    protected void onPause() {
        super.onPause();
        order.dispose();
    }
    //    private void onComplte() {
//
//    }
//
//    private void onError(Throwable throwable) {
//
//    }
//
//    private void onSuccess(FlagModel flagModel) {
//
//    }
}
