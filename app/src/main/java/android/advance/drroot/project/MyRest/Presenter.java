package android.advance.drroot.project.MyRest;

public class Presenter implements Contract.Presenter {

    private Contract.View view;
    Contract.Model model =new Model();


    @Override
    public void attachView(Contract.View view) {

        this.view = view;
        model.attachPresenter(this);
    }

    @Override
    public void register(String username, String password, String email) {

        model.register(username,password,email);
    }

    @Override
    public void registered() {
        view.registered();

    }

    @Override
    public void failed() {
        view.failed();
    }

    @Override
    public void login(String username, String password) {
        model.login(username,password);
    }

    @Override
    public void loginSuccess(String token) {
        model.storeToken(token);
        view.loginSuccess();
    }

}
