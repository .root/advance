package android.advance.drroot.project.IMDB_Retrifit;

import android.advance.drroot.project.IMDB_Retrifit.Models.IMDB;

public class Presenter implements Contract.Presenter {
    Contract.View view ;
    Contract.Model model = new Model();

    public Presenter(Contract.View view) {
        this.view = view;
        model.attachPresenter(this);
    }

    @Override
    public void search(String word) {
        model.search(word);
    }

    @Override
    public void failed() {
        view.failed();
    }

    @Override
    public void movieFound(IMDB result) {
        view.movieFound(result);
    }
}
