package android.advance.drroot.project.MyRest;

import android.advance.drroot.project.MyRest.Models.OrderModel;
import android.advance.drroot.project.MyRest.Models.ResultModel;
import android.advance.drroot.project.MyRest.Models.SuccessEntity;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface MyRestInterface {

    @FormUrlEncoded
    @POST("index.php")
    Call<ResultModel> register
            (@Field("username")String username,
             @Field("password")String password,
             @Field("email")String email);

    @FormUrlEncoded
    @POST("login.php")
    Call<SuccessEntity> login
            (@Field("username")String username,
             @Field("password")String password);

    @FormUrlEncoded
    @POST("login.php")
    Call<List<OrderModel>> getOrder(
            @Header("token")String token
    );

}
