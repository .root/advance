package android.advance.drroot.project.AA;

import android.advance.drroot.project.Kotlin.PublicMethods;
import android.advance.drroot.project.R;
import android.advance.drroot.project.custom_views.MyEditText;
import android.advance.drroot.project.utils.BaseActivity;
import android.advance.drroot.project.utils.Publics;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_aasample)
public class AASampleActivity extends BaseActivity {

    @ViewById
    MyEditText name,family,age ;

    @Click
    void register(){

        String res = name + " " + family + age ;
        PublicMethods.Companion.toast(mContext , res);


    }
}
