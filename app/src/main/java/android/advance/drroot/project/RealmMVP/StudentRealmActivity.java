package android.advance.drroot.project.RealmMVP;

import android.advance.drroot.project.R;
import android.advance.drroot.project.custom_views.MyEditText;
import android.advance.drroot.project.utils.BaseActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.List;

public class StudentRealmActivity extends BaseActivity implements Contract.View ,StudentAdapter.SelectDelete ,StudentAdapter.SelectEdit {

    MyEditText name,family,mobile,age;
    RecyclerView stdlist;
    Contract.Presenter presenter = new Presenter();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_realm);
        bind();
        presenter.attachViwe(this);
        findViewById(R.id.register).setOnClickListener(V->{
            presenter.register(name.text(),family.text(),mobile.text(),age.text());
        });
    }
    void bind(){
        stdlist = findViewById(R.id.stdlist);
        name = findViewById(R.id.name);
        family = findViewById(R.id.family);
        mobile = findViewById(R.id.mobile);
        age = findViewById(R.id.age);

    }

    @Override
    public void onUserLoaded(List<StudentEntity> students) {
        StudentAdapter adapter = new StudentAdapter(
                mContext , students
        );
        stdlist.setAdapter(adapter);
        adapter.setSelectDelete(this);
        adapter.setSelectEdit(this);

    }

    @Override
    public void userRegister() {
        name.setText("");
        family.setText("");
        age.setText("");
        mobile.setText("");
    }

    @Override
    public void onDeleteSelected(StudentEntity students) {
        presenter.userDelet(students);
    }

    @Override
    public void onSelectEdit(StudentEntity students) {
        presenter.userEdit(students);
    }
}
