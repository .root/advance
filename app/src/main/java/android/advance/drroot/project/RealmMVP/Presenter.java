package android.advance.drroot.project.RealmMVP;

import android.widget.Toast;

import java.util.List;

public class Presenter implements Contract.Presenter {
    private Contract.View view;
    Contract.Model model = new Model();

    @Override
    public void attachViwe(Contract.View view) {

        this.view = view;
        model.attachPresenter(this);
        model.loadList();
    }

    @Override
    public void register(String name, String family, String age, String mobile) {
        model.register(name,family,mobile,age);
    }

    @Override
    public void onUserLoaded(List<StudentEntity> students) {
        view.onUserLoaded(students);
    }

    @Override
    public void userRegister() {
        view.userRegister();
        model.loadList();
    }


    @Override
    public void userEdit(StudentEntity students) {
        model.userEdit(students);

    }

    @Override
    public void userDelet(StudentEntity students) {
        model.userDelet(students);
    }
}
