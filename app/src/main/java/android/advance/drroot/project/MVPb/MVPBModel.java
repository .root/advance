package android.advance.drroot.project.MVPb;

import android.advance.drroot.project.utils.Constants;
import android.advance.drroot.project.utils.FlagModels.FlagModels;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class MVPBModel implements MVPBContract.Model {

    MVPBContract.Presenter presenter;
    @Override
    public void attachPresenter(MVPBContract.Presenter presenter) {

        this.presenter = presenter;
    }

    @Override
    public void orderToGetData() {
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        asyncHttpClient.get(Constants.IP_DETAIL_URL, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                presenter.isOnLoading(true);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                presenter.ConnectionFiled();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseData(responseString);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                presenter.isOnLoading(false);
            }
        });
    }
    void parseData(String response){
        Gson gson = new Gson();
        FlagModels result = gson.fromJson(

                response , FlagModels.class
        );
        presenter.orderDataRecived(result);
    }
}
